/********************************************************************
	created:	2013/07/27
	created:	27:7:2013   20:14
	filename: 	C:\Users\Junior\Documents\DotNetWork\KeepWindowsAlive\KeepWindowsAlive.cpp
	file path:	C:\Users\Junior\Documents\DotNetWork\KeepWindowsAlive
	file base:	KeepWindowsAlive
	file ext:	cpp
	author:		Arlef Kaba
	
	purpose:	To prevent windows going into idle mode.
*********************************************************************/
#include <windows.h>

#define ID_TIMER    1

LRESULT CALLBACK WndProc (HWND, UINT, WPARAM, LPARAM);
VOID CALLBACK TimerProc (HWND, UINT, UINT,   DWORD ) ;



int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance,
                    PSTR szCmdLine, int iCmdShow)
{
     static TCHAR szAppName[] = TEXT ("KeepWindowsAlive") ;
     HWND         hwnd ;
     MSG          msg ;
     WNDCLASS     wndclass ;
     
     wndclass.style         = CS_HREDRAW | CS_VREDRAW ;
     wndclass.lpfnWndProc   = WndProc ;
     wndclass.cbClsExtra    = 0 ;
     wndclass.cbWndExtra    = 0 ;
     wndclass.hInstance     = hInstance ;
     wndclass.hIcon         = LoadIcon (NULL, IDI_APPLICATION) ;
     wndclass.hCursor       = LoadCursor (NULL, IDC_ARROW) ;
     wndclass.hbrBackground = (HBRUSH) GetStockObject (WHITE_BRUSH) ;
     wndclass.lpszMenuName  = NULL ;
     wndclass.lpszClassName = szAppName ;
     
     if (!RegisterClass (&wndclass))
     {
          MessageBox (NULL, TEXT ("Program requires Windows NT!"), 
                      szAppName, MB_ICONERROR) ;
          return 0 ;
     }
     
     hwnd = CreateWindow (szAppName, TEXT ("Keep Windows Alive (c) 2013 by Arlef Kaba"),
                          (WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX),
                          CW_USEDEFAULT, CW_USEDEFAULT,
                          500, 300,
                          NULL, NULL, hInstance, NULL) ;
          
     ShowWindow (hwnd, iCmdShow) ;
     UpdateWindow (hwnd) ;
          
     while (GetMessage (&msg, NULL, 0, 0))
     {
          TranslateMessage (&msg) ;
          DispatchMessage (&msg) ;
     }
     return msg.wParam ;
}

VOID CALLBACK TimerProc (HWND hwnd, UINT message, UINT iTimerID, DWORD dwTime)
{	
	//MessageBeep (-1) ;  
	SetThreadExecutionState(ES_DISPLAY_REQUIRED | ES_SYSTEM_REQUIRED | ES_CONTINUOUS);
}

LRESULT CALLBACK WndProc (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
     static BOOL fFlipFlop = FALSE ;
     HBRUSH      hBrush ;
     HDC         hdc ;
     PAINTSTRUCT ps ;
     RECT        rc ;
     
     switch (message)
     {
     case WM_CREATE:
          SetTimer (hwnd, ID_TIMER, 60000, TimerProc) ;
          return 0 ;
	 case WM_PAINT:
		 hdc = BeginPaint(hwnd,&ps);
		 GetClientRect(hwnd,&rc);
		 DrawText(hdc,TEXT("Keeping Windows Alive!!"), -1,&rc, DT_SINGLELINE | DT_CENTER | DT_VCENTER);
		 EndPaint(hwnd,&ps);
		 return 0;


            
     case WM_DESTROY :
          KillTimer (hwnd, ID_TIMER) ;
          PostQuitMessage (0) ;
          return 0 ;
     }
     return DefWindowProc (hwnd, message, wParam, lParam) ;
}

